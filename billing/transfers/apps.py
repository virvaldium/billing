from django.apps import AppConfig


class TransfersConfig(AppConfig):
    name = 'billing.transfers'
