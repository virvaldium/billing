from django.db import models
from django.conf import settings

from currency_converter import CurrencyConverter


class TransferManager(models.Manager):

    def add_start_bonus(self, account, money):
        """Добавление бонусных
        """
        self.create(type=Transfer.TYPES.START_BONUS, recipient=account, received_money=money)
        account.update_balance(money)

    def create_transfer(self, sender, recipient, transferred_money):
        """Логика перевода денежных средств
        """
        received_money = CurrencyConverter().convert(transferred_money, sender.currency, recipient.currency)
        received_money = round(received_money, 3)

        commission = 0
        if sender.owner != recipient.owner:
            commission = round(received_money / 100 * settings.TRANSFER_COMMISSION, 3)
            received_money -= commission

        transfer = self.create(sender=sender, recipient=recipient, transferred_money=transferred_money,
                               received_money=received_money,  commission=commission)

        sender.downgrade_balance(transferred_money)
        recipient.update_balance(received_money)
        return transfer


class Transfer(models.Model):
    class TYPES:
        START_BONUS = 'start_bonus'
        TRANSFER = 'transfer'

    TYPES_CHOICES = (
        (TYPES.START_BONUS, 'start_bonus'),
        (TYPES.TRANSFER, 'transfer'),
    )
    type = models.CharField(max_length=11, blank=False, choices=TYPES_CHOICES, default=TYPES.TRANSFER)
    sender = models.ForeignKey('accounts.Account', related_name='from_account', on_delete=models.CASCADE, null=True)
    recipient = models.ForeignKey('accounts.Account', related_name='to_account', on_delete=models.CASCADE)
    transferred_money = models.FloatField(default=None, null=True)      # отправленные
    received_money = models.FloatField(default=0)                       # полученные
    commission = models.FloatField(default=0)                           # комиссия в валюте счета получателя
    date_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = "transfers"
        db_table = "transfers_transfer"
        ordering = ['date_at']

    objects = TransferManager()
