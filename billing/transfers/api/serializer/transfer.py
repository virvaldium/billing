from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField, FloatField, ValidationError

from billing.accounts.models import Account
from billing.transfers.models import Transfer


class TransferSerializer(ModelSerializer):
    sender = PrimaryKeyRelatedField(queryset=Account.objects.all())
    recipient = PrimaryKeyRelatedField(queryset=Account.objects.all())
    transferred_money = FloatField(min_value=0)

    def validate(self, attrs):
        # проверим, что денег на счету хватает
        balance = attrs['sender'].balance
        transferred_money = attrs['transferred_money']
        if (balance - transferred_money) < 0:
            raise ValidationError({'balance': 'not enough money in the account'})
        return attrs

    def create(self, validated_data):
        transfer = Transfer.objects.create_transfer(**validated_data)
        return transfer

    class Meta:
        model = Transfer
        fields = '__all__'
