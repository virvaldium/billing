from django.db.models import Q
from django.db import transaction, IntegrityError

from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from django.http import HttpResponseBadRequest

from billing.transfers.models import Transfer
from billing.common.pagination import Pagination
from billing.transfers.api.serializer import TransferSerializer


class TransferViewSet(GenericViewSet, CreateModelMixin, ListModelMixin, RetrieveModelMixin):
    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = Pagination

    def get_queryset(self):
        """Отдаем пользователю, только его переводы
        """
        queryset = super(TransferViewSet, self).get_queryset()
        return queryset.filter(Q(sender__owner=self.request.user) | Q(recipient__owner=self.request.user))

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            with transaction.atomic():
                self.perform_create(serializer)
        except IntegrityError:
            # TODO тут выставляется обработчик исключения
            raise HttpResponseBadRequest    # это заглушка

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=HTTP_201_CREATED, headers=headers)
