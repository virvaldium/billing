from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from billing.transfers.api.endpoint import TransferViewSet

transfers_router = DefaultRouter()
transfers_router.register(r'transfers', TransferViewSet)


urlpatterns = [
    url(r'^', include(transfers_router.urls)),
]
