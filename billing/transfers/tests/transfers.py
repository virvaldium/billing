from rest_framework import status
from rest_framework.test import APITestCase

from billing.users.models import User
from billing.accounts.models import Account
from billing.transfers.models import Transfer

TRANSFER_USER_URL = '/api/v1/transfers/'


class TransferTest(APITestCase):
    fixtures = ['fixtures/users.json', 'fixtures/accounts.json', 'fixtures/tokens.json', 'fixtures/transfers.json']

    def setUp(self):
        user = User.objects.get(username='user')
        self.client.force_authenticate(user=user)

    def test_get_transfers_list(self):
        response = self.client.get(TRANSFER_USER_URL, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        self.assertTrue('items' in response.data.keys())
        self.assertTrue('meta' in response.data.keys())
        self.assertEqual(len(response.data['items']), 1, response.content)

    def test_transfer_another_account(self):
        data = {
            "sender": 1,
            "recipient": 4,
            "transferred_money": 10
        }
        response = self.client.post(TRANSFER_USER_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        self.assertEqual(Transfer.objects.count(), 3)
        self.assertEqual(response.data['received_money'], 9.99)
        self.assertEqual(response.data['commission'], 0.01)

    def test_transfer_between_account(self):
        data = {
            "sender": 1,
            "recipient": 2,
            "transferred_money": 10
        }
        response = self.client.post(TRANSFER_USER_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        self.assertEqual(Transfer.objects.count(), 3)
        self.assertEqual(response.data['transferred_money'], 10, response.content)
        sender_account = Account.objects.get(pk=data['sender'])
        self.assertEqual(sender_account.balance, 90, response.content)

    def test_incorrect_amount_transfer(self):
        data = {
            "sender": 1,
            "recipient": 2,
            "transferred_money": 1000   # incorrect amount
        }
        response = self.client.post(TRANSFER_USER_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content)
        self.assertEqual(response.data['balance'][0], 'not enough money in the account', response.content)

    def test_transfer_to_incorrect_account(self):
        data = {
            "sender": 1,
            "recipient": 100500,        # incorrect account
            "transferred_money": 10
        }
        response = self.client.post(TRANSFER_USER_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content)
        self.assertEqual(response.data['recipient'][0], 'Invalid pk "{0}" - object does not exist.'.
                         format(data['recipient']), response.content)
