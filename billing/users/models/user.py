from django.conf import settings

from django.contrib.auth.models import AbstractUser, UserManager
from billing.accounts.models import Account
from billing.transfers.models import Transfer


class BillingUserManager(UserManager):

    def create_user(self, username, email=None, password=None, **extra_fields):
        user = super().create_user(username, email=email, password=password, **extra_fields)

        # Create user accounts
        usd_account = Account.objects.create(owner=user, currency=Account.CURRENCY.USD)
        Account.objects.create(owner=user, currency=Account.CURRENCY.CNY)
        Account.objects.create(owner=user, currency=Account.CURRENCY.EUR)

        # add money to the starting account
        Transfer.objects.add_start_bonus(usd_account, settings.START_BONUS_USD)

        return user


class User(AbstractUser):

    def __str__(self):
        return self.email

    class Meta:
        app_label = "users"
        db_table = "users_user"

    objects = BillingUserManager()
