from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'billing.users'
