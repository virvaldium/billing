from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.status import HTTP_201_CREATED
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin
from rest_framework.authtoken.models import Token

from billing.users.models import User
from billing.users.api.serializer import UserSerializer
from billing.common.pagination import Pagination


class UserViewSet(GenericViewSet, CreateModelMixin):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
    pagination_class = Pagination

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        # Generate token after registration
        token = Token.objects.create(user=serializer.instance)
        json = serializer.data
        json['token'] = token.key
        return Response(json, status=HTTP_201_CREATED, headers=headers)
