from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from billing.users.models import User
from billing.accounts.models import Account
from billing.transfers.models import Transfer

CREATE_USER_URL = '/api/v1/users/'


class UserTest(APITestCase):

    def test_create_user(self):
        """Ensure we can create a new user and a valid token is created with it.
        """
        data = {
            'username': 'foobar',
            'email': 'foobar@example.com',
            'password': 'somepassword'
        }

        response = self.client.post(CREATE_USER_URL, data, format='json')

        # And that we're returning a 201 created code.
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
        # Additionally, we want to return the username and email upon successful creation.
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(response.data['username'], data['username'], response.content)
        self.assertEqual(response.data['email'], data['email'], response.content)

        # Check authorization token
        user = User.objects.latest('id')
        token = Token.objects.get(user=user)
        self.assertEqual(response.data['token'], token.key)

        # Check created accounts
        self.assertEqual(Account.objects.filter(owner=user).count(), 3)

        # Check created transfers
        Transfer.objects.filter(recipient__owner=user)

    def test_incorrect_registration(self):
        data = {

        }
        response = self.client.post(CREATE_USER_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content)
        self.assertEqual(response.data['username'][0], 'This field is required.', response.content)
        self.assertEqual(response.data['email'][0], 'This field is required.', response.content)
        self.assertEqual(response.data['password'][0], 'This field is required.', response.content)


