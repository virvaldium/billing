from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from billing.users.models import User

AUTH_USER_URL = '/api/v1/auth-token/'


class AuthTest(APITestCase):

    def setUp(self):
        User.objects.create_user(username='test_user', email='test@example.com', password='test_password')

    def test_get_token(self):
        data = {
            "username": "test_user",
            "password": "test_password"
        }

        response = self.client.post(AUTH_USER_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check authorization token
        user = User.objects.latest('id')
        token = Token.objects.get(user=user)
        self.assertEqual(response.data['token'], token.key)

    def test_incorrect_password(self):
        data = {
            "username": "test_user",
            "password": "incorrect_password"
        }

        response = self.client.post(AUTH_USER_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
