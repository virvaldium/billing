from django.contrib import admin
from django.conf.urls import include, url

from billing.users import urls as users_urls
from billing.accounts import urls as accounts_urls
from billing.transfers import urls as transfers_urls


api_endpoint_view = [
    url(r'^(?P<version>(v1))/', include(users_urls)),
    url(r'^(?P<version>(v1))/', include(accounts_urls)),
    url(r'^(?P<version>(v1))/', include(transfers_urls)),
    ]

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^api/', include(api_endpoint_view)),
]
