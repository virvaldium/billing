import os

from configurations import Configuration

from billing.settings.apps import AppsMixin
from billing.settings.database import DatabaseMixin
from billing.settings.path import PathMixin


class Common(AppsMixin, DatabaseMixin, PathMixin, Configuration):
    @classmethod
    def pre_setup(cls):
        cls.DOTENV = os.path.join(cls.BASE_DIR, '.env')
        super(Common, cls).pre_setup()


class Dev(Common):
    """
    The in-development settings and the default configuration.
    """
    DEBUG = True

    Common.INSTALLED_APPS += [
    ]
