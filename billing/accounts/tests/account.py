from rest_framework import status
from rest_framework.test import APITestCase

from billing.users.models import User
from billing.accounts.models import Account

ACCOUNTS_USER_URL = '/api/v1/accounts/'


class AccountTest(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='test_user', email='test@example.com', password='test_password')
        self.client.force_authenticate(user=user)

    def test_get_user_accounts(self):
        response = self.client.get(ACCOUNTS_USER_URL, content_type="application/json;charset=UTF-8")
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        self.assertTrue('items' in response.data.keys())
        self.assertTrue('meta' in response.data.keys())

    def test_create_new_account(self):
        data = {
            "currency": "USD"
        }
        response = self.client.post(ACCOUNTS_USER_URL, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        self.assertEqual(Account.objects.count(), 4)  # 3 стартовых счета + который создали мы
