from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from billing.accounts.api.endpoint import AccountViewSet

accounts_router = DefaultRouter()
accounts_router.register(r'accounts', AccountViewSet)


urlpatterns = [
    url(r'^', include(accounts_router.urls)),
]
