from rest_framework.serializers import ModelSerializer, HiddenField, CurrentUserDefault, ChoiceField

from billing.accounts.models import Account


class AccountSerializer(ModelSerializer):
    currency = ChoiceField(choices=Account.CURRENCY_CHOICES)
    owner = HiddenField(default=CurrentUserDefault())

    class Meta:
        model = Account
        fields = '__all__'
