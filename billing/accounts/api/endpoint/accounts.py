from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin

from billing.accounts.models import Account
from billing.common.pagination import Pagination
from billing.accounts.api.serializer import AccountSerializer


class AccountViewSet(GenericViewSet, CreateModelMixin, ListModelMixin, RetrieveModelMixin):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = Pagination

    def get_queryset(self):
        """Отдаем пользователю, только его счета
        """
        queryset = super(AccountViewSet, self).get_queryset()
        return queryset.filter(owner=self.request.user)
