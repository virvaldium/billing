from django.db import models
from django.db.models import F

from django.core.validators import MinValueValidator


class Account(models.Model):
    """модель описывает счета, которые есть у клиента
    """
    class CURRENCY:
        USD = 'USD'
        CNY = 'CNY'
        EUR = 'EUR'

    CURRENCY_CHOICES = (
        (CURRENCY.USD, 'USD'),
        (CURRENCY.CNY, 'CNY'),
        (CURRENCY.EUR, 'EUR'),
    )

    owner = models.ForeignKey('users.User', on_delete=models.CASCADE)
    currency = models.CharField(max_length=3, blank=False, choices=CURRENCY_CHOICES, default=CURRENCY.USD)
    balance = models.FloatField(validators=[MinValueValidator(0)], default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = "accounts"
        db_table = "accounts_account"
        ordering = ['created_at']

    def update_balance(self, money):
        """добавим на баланс
        """
        # такой подход позволяет блокировать строку до тех пор пока не будет вызван update
        Account.objects.select_for_update().filter(pk=self.pk).update(balance=F('balance') + money)

    def downgrade_balance(self, money):
        """вычитаем с баланса
        """
        # такой подход позволяет блокировать строку до тех пор пока не будет вызван update
        Account.objects.select_for_update().filter(pk=self.pk).update(balance=F('balance') - money)
        # TODO тут стоит сделать проверку на то, что текущий баланс не равен нулю
